

import tensorflow as tf

matrix1 = tf.constant([[2.,3.]])
matrix2 = tf.constant([[3],[2.]])

product = tf.matmul(matrix1,matrix2)

with tf.Session() as sess:
    result = sess.run(product)
    print(result)

# example 2
sess = tf.InteractiveSession()

x = tf.Variable([1.0,2.0])
a = tf.constant([3.0,4.0])

x.initializer.run()
sub = tf.subtract(x,a )
print(sub.eval())

#example 3
input1 = tf.constant(3.0)
input2 = tf.constant(2.0)
input3 = tf.constant(5.0)

intermed = tf.add(input2, input3)
mul = tf.multiply(input1, intermed)

with tf.Session() as sess:
    result = sess.run([mul,intermed])
    print(result)


#example 4
input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)

output = tf.multiply(input1,input2)
with tf.Session() as sess:
    print(sess.run([output], feed_dict={input1:[8.], input2:[6.0]}))


tf.random_normal