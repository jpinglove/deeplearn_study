
import os
import glob
import math

working_path = os.getcwd()
print("\nworking path: ", working_path)
print(glob.glob("*.py"))

print(math.pi)

file = open("text.txt","r")
data = file.readline()
if data.find("llo") != -1 :
    print("find target string")
else:
    print("do not find the string")

print(data,end="|")
file.seek(0)
print(file.readline())

try:
    pf = open("write.txt","w")
    try:
        pf.write("this is some text for written in to file.")
        # pf.close()
    except ValueError:
        print("write file data failed")
    finally:
        pf.close()
except IOError:
    print("open write txt file failed")