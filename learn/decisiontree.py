# -*- coding:utf-8 -*-
"""
@author:Levy
@file:decisiontree.py
@time:2018/1/723:03
"""

'''from sklearn.feature_extraction import Dictvetorizer
'''
from sklearn.feature_extraction import  DictVectorizer
import csv
from sklearn import preprocessing
from sklearn import tree
from sklearn.externals.six import StringIO

allelectionicsData = open(r'e:\book01.csv')
reader = csv.reader(allelectionicsData)
# header = reader.next()
header = next(reader)
print(header)

index = 0

# for line in reader:
#     index = index + 1
#     if index == 1 :
#         continue
#     else:
#         print(line)

featurelist = []
labellist = []
for row in reader:
    labellist.append(row[len(row)-1])
    print(labellist)
    rowDict = {}
    for i in range(1,len(row)-1):
        rowDict[header[i]] = row[i]
    featurelist.append(rowDict)
print(featurelist)

vec = DictVectorizer()
dummyX = vec.fit_transform(featurelist).toarray()
print(dummyX)

lb = preprocessing.LabelBinarizer()
dummyY =lb.fit_transform(labellist)
print("dummyY: ",dummyY)

clf01 = tree.DecisionTreeClassifier(criterion='entropy')
clf = clf01.fit(dummyX,dummyY)
print("clf: ", str(clf))

with open('d:\cc.dot','w') as f:
    f = tree.export_graphviz(clf, feature_names=vec.get_feature_names(),out_file=f)
oneRowX = dummyX[0,:]
print("oneRowX:", str(oneRowX))
newRowX = oneRowX
newRowX[0] = 1
newRowX[2] = 0
print('newRowY:', str(newRowX))
print(help(clf.predict))

predictedY = clf.predict(newRowX.reshape(1,-1))
print((str(predictedY)))

print("==== programe end ==============")