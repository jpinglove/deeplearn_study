# -*- coding:utf-8 -*-
"""
@author:Levy
@file:svm01.py
@time:2018/1/130:47
"""

from sklearn import svm
import numpy

x = [[2,0],[1,1],[2,3]]
y = [0,0,1]

clf = svm.SVC(kernel='linear')
clf.fit(x,y)

print(clf)
print(clf.support_vectors_)
print(clf.support_)
print(clf.n_support_)
arr = numpy.array([2,.0])
print(clf.predict(arr.reshape(1,-1)))

