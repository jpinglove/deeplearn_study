
import sys

def new_print(arg):
    "自定义的打印函数"
    print("new print fun = ", arg)

def multip(arg) :
    "参数*10 返回结果"
    return  arg*10

list = [1,2,3,4,5]
it = iter(list)
while True:
    try:
        new_print(multip(next(it)))
    except StopIteration:
        sys.exit()

print("program finish")